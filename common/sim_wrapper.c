#include "sim_wrapper.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <simavr/sim_gdb.h>
#include <simavr/sim_time.h>


#define SIM_INFO(msg, ...) L_INFO("SIM: " msg, ##__VA_ARGS__)
#define SIM_WARNING(msg, ...) L_WARNING("SIM: " msg, ##__VA_ARGS__)


static bool checkGracefullyQuit(avr_t *avr) {
    char *reason;
    if (!avr->sreg[S_I] && avr->state == cpu_Running && avr->flash[avr->pc] == 0xff && avr->flash[avr->pc + 1] == 0xcf)
        reason = "rjmp .-2 with interrupts off";
    else if (!avr->sreg[S_I] && avr->state == cpu_Sleeping)
        reason = "sleeping with interrupts off";
    else return false;

    SIM_INFO("Quitting gracefully (%s)", reason);
    avr->state = cpu_Done;
    return true;
}


#if IS_WIN
#include <winnt.h>
#define CLOCK_MONOTONIC 0
static int clock_gettime(int id, struct timespec *spec) {
    FILETIME ft;
    GetSystemTimePreciseAsFileTime(&ft);
    ULARGE_INTEGER li;
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;
    uint64_t t = li.QuadPart;
    t      -= 116444736000000000i64;            //1jan1601 to 1jan1970
    spec->tv_sec  = t / 10000000i64;            //seconds
    spec->tv_nsec = t % 10000000i64 * 100;      //nano-seconds
    return 0;
}
#endif

#define nanos_per_sec 1000000000ul
static uint64_t timespecToNanos(struct timespec ts) {
    return ts.tv_sec * nanos_per_sec + ts.tv_nsec;
}
static struct timespec nanosToTimespec(uint64_t nanos) {
    return (struct timespec) {
            .tv_sec = nanos / nanos_per_sec,
            .tv_nsec = nanos % nanos_per_sec
    };
}
static uint64_t getCurrentTime(void) {
    struct timespec ts = {.tv_sec = 0, .tv_nsec = 0};
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return timespecToNanos(ts);
}
//static void sleepTill(uint64_t time) {
//    struct timespec ts = nanosToTimespec(time);
//    int err;
//    if ((err = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL))) {
//        if (err != EINTR) SIM_WARNING("clock_nanosleep error (%d)", err);
//    }
//}


static void *sim_thread_routine(void *param) {
    sim_t *sim = param;
    avr_t *avr = sim->avr;
    
    if (avr->gdb != NULL) SIM_INFO("GDB: Available on port %d...", avr->gdb_port);
    printf("\n-->\n");

    uint64_t cyclesPerCheck = 100000;
    uint64_t cycles = 0;
    uint64_t lastCheckTime = sim->startTime = getCurrentTime();
    int state = cpu_Running;
    while (state != cpu_Done && state != cpu_Crashed) {
        state = avr_run(avr);
        checkGracefullyQuit(avr);
        if (++cycles >= cyclesPerCheck) {
            uint64_t targetTime = lastCheckTime + sim->targetCycleDelta * cycles;
            uint64_t currentTime;
            if (sim->tryCTCorrection) while ((currentTime = getCurrentTime()) < targetTime);
            else currentTime = getCurrentTime();
            sim->currentTime = currentTime;
            sim->lastCycleDelta = (currentTime - lastCheckTime) / cycles;
            cycles = 0;
            lastCheckTime = currentTime;
        }
    }
    sim->isDone = true;
    SIM_INFO("Done (state: %d)", state);
    pthread_detach(pthread_self());
    return NULL;
}

void sim_init(sim_t *sim) {
    SIM_INFO("Initializing... (m=%s,f=%d)", sim->mmcu, sim->frequency);
    avr_t *avr = sim->avr = avr_make_mcu_by_name(sim->mmcu);
    if (!avr) L_ERROR_EXIT(sim->s, "SIM: AVR '%s' not known", sim->mmcu);
    avr_init(avr);
    avr_load_firmware(avr, &sim->firmware);
    if (sim->frequency) avr->frequency = sim->frequency;
    sim->frequency = avr->frequency;
    sim->avr->avcc = sim->avr->vcc = 5000;

    if (sim->gdbPort) {
        avr->gdb_port = sim->gdbPort;
        avr->state = cpu_Stopped;
        avr_gdb_init(avr);
        SIM_INFO("Enabled GDB on port %d", sim->gdbPort);
    }
    sim->isDone = false;
    sim->targetCycleDelta = avr_cycles_to_nsec(avr, 1);
#if IS_WIN
    SIM_INFO("Initialized (cd:%llu,ctc:%i)", sim->targetCycleDelta, sim->tryCTCorrection);
#else
    SIM_INFO("Initialized (cd:%lu,ctc:%i)", sim->targetCycleDelta, sim->tryCTCorrection);
#endif
}
void sim_start(sim_t *sim) {
    pthread_create(&sim->thread, NULL, sim_thread_routine, sim);
}
void sim_reset(sim_t *sim) {
    SIM_INFO("Resetting...");
    avr_reset(sim->avr);
}
void sim_waitTillDone(sim_t *sim) {
    pthread_join(sim->thread, NULL);
}
