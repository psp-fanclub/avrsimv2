#ifndef _BOARD_XML_LED_C
#define _BOARD_XML_LED_C

#include "board_xml_common.c"

#include <stdbool.h>

#include "../components/led.h"
#include "../components/led_gl.h"


typedef struct {
    led_t led;
    bool highOn;
    board_xml_irq_t in;
} board_xml_led_t;

typedef struct {
    led_t *led;
    GLcolor32 color;
} board_xml_led_layout_t;


static BOARD_XML_COMP_INIT(board_xml_led_comp_init, data, board, avr) {
    board_xml_data_cast(board_xml_led_t, led, data);
    led_init(&led->led, avr, led->highOn);
    led_connect(&led->led, board_xml_irq_get(avr, led->in));
}

static BOARD_XML_LAYOUT_ONLAYOUT(board_xml_led_layout_onLayout, data, board) {
    board_xml_data_cast(board_xml_led_layout_t, led, data);
    glLSimpleObject(led_gl_draw(led->led, led->color), led_gl_getSize());
}




static void board_xml_led_parse(board_xml_t *board, xmlNode *node, board_xml_comp_t *comp) {
    comp->init = board_xml_led_comp_init;
    board_xml_led_t *led = comp->data = calloc(1, sizeof(board_xml_led_t)); // free
    led->in = board_xml_irq_parse(xmlUtilsFindChild(node, "in"));
    led->highOn = board_xml_bool_parse(xmlUtilsFindAttrVal(node, "highOn"), false);
}


static void board_xml_led_layout_parse(board_xml_t *board, xmlNode *node, board_xml_layout_t *layout) {
    layout->onLayout = board_xml_led_layout_onLayout;
    board_xml_led_layout_t *led = layout->data = calloc(1, sizeof(board_xml_led_layout_t)); // free
    led->led = &board_xml_findCompDataByAttr(board_xml_led_t, board, xmlUtilsFindAttrVal(node, "id"))->led;
    led->color = board_xml_color_parse(xmlUtilsFindAttrVal(node, "color"), 0xFFFFFFFF);
}


#endif // _BOARD_XML_LED_C