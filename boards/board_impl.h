#ifndef _BOARD_H
#define _BOARD_H

#include <simavr/sim_irq.h>
#include <simavr/avr_spi.h>
#include <simavr/avr_ioport.h>

#include "../common/compat.h"
#include "../common/board.h"
#include "../common/log.h"

// P = IOPORT_IRQ_PIN...
#define IRQ_IOPORT(avr, N, P) avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ(N), P)
#define IRQ_SPI(avr, N, P) avr_io_getirq(avr, AVR_IOCTL_SPI_GETIRQ(N), P)

#ifdef __cplusplus
extern "C" {
#endif

EXPORT BOARD_LOAD_F(board, argc, argv);

static void board_configure_log(board_t *board) {
    logger = board->logger;
}
    
#ifdef __cplusplus
}
#endif


#endif //_BOARD_H
