#include "spi.h"

#if SPI_SLAVE_BITBANG

#include <stdio.h>


#define SPI_WORD_SIZE 8

static void spi_slave_onPreBit(spi_slave_t *s) {
    size_t bit = SPI_WORD_SIZE - 1 - (s->bit_counter % SPI_WORD_SIZE);
    
//    printf("Trace: %s(): bit=%zu\n", __func__, bit);
    
    bool wordStart = bit == SPI_WORD_SIZE - 1;
    if (wordStart) {
        s->word = s->onPreWord(s->bit_counter / SPI_WORD_SIZE, s->param);
    }
    s->so_state = s->word & (1 << bit);
    
    
    avr_raise_irq(s->irq + IRQ_SPI_SLAVE_SO, s->so_state);
}
static void spi_slave_onBit(spi_slave_t *s) {
    size_t bit = SPI_WORD_SIZE - 1 - s->bit_counter % SPI_WORD_SIZE;
    s->word = (s->word & ~(1 << bit)) | (s->si_state << bit);

    bool wordComplete = bit == 0;
    if (wordComplete) {
        s->onWord(s->bit_counter / SPI_WORD_SIZE, s->word, s->param);
        s->word = 0;
    }
    
    s->bit_counter++;
}
static void spi_slave_clk_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
//    printf("Trace: %s(): v=%d\n", __func__, value);
    spi_slave_t *s = (spi_slave_t *) param;
    if (!s->selected) return;
    bool isTrailing = (value != 0) == s->polarity;
    if (s->phase != isTrailing) spi_slave_onPreBit(s);
    else spi_slave_onBit(s);
}
static void spi_slave_si_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
    spi_slave_t *s = (spi_slave_t *) param;
    s->si_state = value;
}
static void spi_slave_cs_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
//    printf("Trace: %s(): v=%d\n", __func__, value);
    spi_slave_t *s = (spi_slave_t *) param;
    s->selected = !value;
    if (!s->selected) return;
    s->bit_counter = 0;
    if (!s->phase) spi_slave_onPreBit(s);
}

void spi_slave_init(spi_slave_t *s, avr_t *avr, const char *name) {
    s->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_SPI_SLAVE_COUNT, &name);
    avr_irq_register_notify(s->irq + IRQ_SPI_SLAVE_CLK, spi_slave_clk_hook, s);
    avr_irq_register_notify(s->irq + IRQ_SPI_SLAVE_SI , spi_slave_si_hook , s);
    avr_irq_register_notify(s->irq + IRQ_SPI_SLAVE_CS , spi_slave_cs_hook , s);
    s->avr = avr;
    s->name = name;
}
void spi_slave_connect(spi_slave_t *s, avr_irq_t *clk, avr_irq_t *si, avr_irq_t *so, avr_irq_t *cs) {
    avr_connect_irq(clk, s->irq + IRQ_SPI_SLAVE_CLK);
    avr_connect_irq(si , s->irq + IRQ_SPI_SLAVE_SI );
    avr_connect_irq(s->irq + IRQ_SPI_SLAVE_SO, so);
    avr_connect_irq(cs , s->irq + IRQ_SPI_SLAVE_CS );
}

#else


static const char *irq_names[IRQ_SPI_SLAVE_COUNT] = {
        [IRQ_SPI_SLAVE_MISO] = "<spi_slave.miso",
        [IRQ_SPI_SLAVE_MOSI] = ">spi_slave.mosi",
        [IRQ_SPI_SLAVE_CS] = ">spi_slave.cs"
};

static void spi_slave_spi_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
    spi_slave_t *s = (spi_slave_t *) param;
    if (!s->selected) return;
    uint8_t word = s->onPreWord(s->word_counter, s->param);
    s->onWord(s->word_counter, value, s->param);
    avr_raise_irq(s->irq + IRQ_SPI_SLAVE_MISO, word);
    s->word_counter++;
}
static void spi_slave_cs_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
    spi_slave_t *s = (spi_slave_t *) param;
    s->selected = !value;
    if (s->selected) s->word_counter = 0;
}

void spi_slave_init(spi_slave_t *s, avr_t *avr) {
    s->avr = avr;
    s->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_SPI_SLAVE_COUNT, irq_names);
    avr_irq_register_notify(s->irq + IRQ_SPI_SLAVE_MOSI, spi_slave_spi_hook, s);
    avr_irq_register_notify(s->irq + IRQ_SPI_SLAVE_CS, spi_slave_cs_hook, s);
}
void spi_slave_connect(spi_slave_t *s, avr_irq_t *miso, avr_irq_t *mosi, avr_irq_t *cs) {
    avr_connect_irq(s->irq + IRQ_SPI_SLAVE_MISO, miso);
    avr_connect_irq(mosi, s->irq + IRQ_SPI_SLAVE_MOSI);
    avr_connect_irq(cs, s->irq + IRQ_SPI_SLAVE_CS);
}


#endif