#include "led.h"


static const char *irq_names[IRQ_LED_COUNT] = {
        [IRQ_LED_IN] = ">led.in"
};

static void led_pin_changed_hook(struct avr_irq_t *irq, uint32_t value, void *param) {
    led_t *l = (led_t *) param;
    l->isOn = l->highOn == (value != 0);
}

void led_init(led_t *l, avr_t *avr, bool highOn) {
    l->irq = avr_alloc_irq(&avr->irq_pool, 0, IRQ_LED_COUNT, irq_names);
    avr_irq_register_notify(l->irq + IRQ_LED_IN, led_pin_changed_hook, l);
    l->avr = avr;
    l->highOn = highOn;
    l->isOn = !highOn;
}
void led_connect(led_t *l, avr_irq_t *in) {
    avr_connect_irq(in, l->irq + IRQ_LED_IN);
}

